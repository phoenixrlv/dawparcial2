
package controlador;

import java.io.IOException;
import java.util.HashSet;
import modelo.Alumno;
import modelo.Modelo;
import vista.Vista;


/**
 *
 * @author paco
 */
public class Controlador {

    private Vista vista;
    private Modelo modelo;
    
    Controlador(Vista vista_, Modelo modelo_) throws IOException {
            vista = vista_;
            modelo = modelo_;
            menuControlador();
    }

    private void menuControlador() throws IOException {
      
     String opcion;
     Alumno alumno;
     String id;
        
      do {

          
       // (3.1) Declarar parametro y llamar a la función  vista.menuVista con el paramtro por      
      // referencia. Pasar opcion a minuscual.
          
      String parametro[] = new String[1];    
      vista.menuVista(parametro);
      opcion = parametro[0];
      opcion = opcion.toLowerCase();
      

      switch (opcion) {

        case "e": // Exit
          vista.exit();
          break;

        case "c": // Crear/AñadirAñadir
          alumno = vista.getAlumno();
          id = Integer.toString(modelo.getId() + 1);
          alumno.setId(id);
          modelo.create(alumno);

          break;
        case "r": // Read / Obtener / Listar
          HashSet alumnos = new HashSet();
          alumnos = modelo.read();
          vista.show(alumnos);
          break;

        default:
          vista.error();
          break;
      }

    } while ( ! opcion.equals("e") );
        
    }
    
 
}
