

// (1) completar los package y los import
package controlador;
import java.io.IOException;
import modelo.Modelo;
import modelo.ModeloArrayList;
import modelo.ModeloHashset;
import vista.Vista;

public class Main {
    public static void main(String[] args) throws IOException {        
        Modelo modelo = new ModeloArrayList(); // (2) Instanciar el ModeloArrayList
        Vista vista = new Vista();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
