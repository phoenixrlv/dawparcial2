
package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import modelo.Alumno;
import modelo.Modelo;

public class Vista {

    
    
    // Por referencia
    public void menuVista(String opcion[]) throws IOException {

        System.out.println("MENU CRUD ");
        System.out.println("e. exit ");
        System.out.println("c. create ");
        System.out.println("r. read ");
        System.out.print("Opción:  ");
        opcion[0] = leerLinea();

    }

    // Por valor
    public String menuVista() throws IOException {

        String opcion;

        System.out.println("MENU CRUD ");
        System.out.println("e. exit ");
        System.out.println("c. create ");
        System.out.println("r. read ");
        System.out.print("Opción:  ");
        opcion = leerLinea();

        return opcion;

    }

    private String leerLinea() throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        String linea = "";
        linea = br.readLine();
        return linea;
    }

    public Alumno getAlumno() {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        Alumno a;
        String nombre = "";
        String matricula = "";
        String linea = "";
        int edad = 0;
        boolean error = true;
        boolean esvalido = true;
        String id;

        System.out.println("ENTRADA DE DATOS");

        System.out.print("Nombre: ");
        try {
            nombre = leerLinea();
        } catch (IOException ex) {
            Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);
        }

        error = true;
        while (error == true) {

            System.out.print("Matricula: ");
            try {
                matricula = br.readLine();
                esvalido = validarMatricula(matricula);
                if (esvalido) {
                    error = false;
                } else {
                    System.out.println("Error: Matricula no valida");
                    error = true;
                }

            } catch (IOException ex) {
                Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);

            }

        } // while

        a = new Alumno("", nombre, matricula);

        return a;
    }

    public void readAlumno(Alumno alumno) {
        System.out.println("Id: " + alumno.getId());
        System.out.println("Nombre: " + alumno.getNombre());
        System.out.println("Matricula: " + alumno.getMatricula());
    }

    public void exit() {
        System.out.println("Fin ");
    }

    public void error() {
        System.out.println("Error ");
    }

    private boolean validarMatricula(String matricula) {

        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(matricula);
        return m.matches();
    }

    public void show(HashSet alumnos) {
        
        Alumno alumno;
        Iterator it = alumnos.iterator();
       
        System.out.println("SALIDA DE DATOS");
        while ( it.hasNext() ) {
            alumno = (Alumno) it.next();
            readAlumno(alumno);
        }
    }

}
