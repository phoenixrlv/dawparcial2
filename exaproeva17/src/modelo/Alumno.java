
package modelo;

// (3) Completar la cabecera de la clase Alumno
public class Alumno extends Persona {
    
    private String matricula;

    public Alumno(String id_, String nombre_, String matricula_) {
          id = id_;
          nombre = nombre_;
          matricula = matricula_;
    }

    public String getMatricula() {
        return matricula;
    }


    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    
}
