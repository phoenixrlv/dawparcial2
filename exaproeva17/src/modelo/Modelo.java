
package modelo;
import java.util.HashSet;
import vista.Vista;
public interface Modelo {
    public int getId();
    public void create(Alumno alumno);
    public HashSet read();   
}
