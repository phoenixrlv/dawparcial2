
package modelo;

import java.util.HashSet;
import vista.Vista;


public class ModeloHashset implements Modelo {
    
   HashSet alumnos = new HashSet();
    int id = 0;
    
    @Override
    public int getId() {
        return id;
    }

    @Override
    public void create(Alumno alumno) {
        alumnos.add(alumno);
         id++;
    }


    public HashSet read() {
       return alumnos;
        }

}
