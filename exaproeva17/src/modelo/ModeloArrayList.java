
package modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class ModeloArrayList implements Modelo {
    
    ArrayList<Alumno> alumnos = new ArrayList<Alumno>();
    int id = 0;
    
    @Override
    public int getId() {
        return id;
    }

    @Override
    public void create(Alumno alumno) {
        alumnos.add(alumno);
        id++;
    }

    @Override
    public HashSet read() {
        HashSet alumnoshs = new HashSet();
        Alumno alumno;
        Iterator it = alumnos.iterator();
        while ( it.hasNext() ) {
            alumno = (Alumno) it.next();
            alumnoshs.add(alumno);
        }
        return alumnoshs;
    }
 
}
